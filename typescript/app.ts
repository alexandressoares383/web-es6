let ola = (nome: string, sobrenome: string) => {
    console.log('Olá ' + nome + sobrenome);
}

ola('Alexandre', 'Soares');

/** Variaveis */
let mensagem: String = 'Seja bem-vindo!'
let temporadasFriends: Number = 10
let estudarAngular: Boolean = true
let listaDeFrutas: Array<String> = ['Uva', 'Banana', 'Abacaxi']
let listaDeFrutas2: String[] = ['Uva', 'Banana', 'Abacaxi']
let notasDasProvas: Array<Number> = [7.5, 8, 9]
let notasDasProvas2: Number[] = [7.5, 8, 9]