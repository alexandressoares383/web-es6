//import ConcessionariaDao from './ConcessionariaDao';
//import PessoaDao from './PessoaDao';

import Dao from './Dao';
import Concessionaria from './Concessionaria';
import Pessoa from './Pessoa';

// let dao: ConcessionariaDao = new ConcessionariaDao();
// let dao2: PessoaDao = new PessoaDao();

let concessionaria = new Concessionaria('', []);
let dao3: Dao<Concessionaria> = new Dao<Concessionaria>();
dao3.inserir(concessionaria);

let pessoa: Pessoa = new Pessoa('', '');
let dao4: Dao<Pessoa> = new Dao<Pessoa>();
dao4.inserir(pessoa);