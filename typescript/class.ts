import Carro from './Carro';
import Pessoa from './Pessoa';
import Concessionaria from './Concessionaria';

// Criar carros
let carroA = new Carro('dodge journey', 4)
let carroB = new Carro('veloster', 3)
let carroC = new Carro('cerato', 4)

// Montar a lista de carros de concessionária
let listaDeCarros: Carro[] = [carroA, carroB, carroC]
let concessionaria = new Concessionaria('Av. Paulista', listaDeCarros)

// Comprar Carro 
let cliente = new Pessoa('João', 'veloster')

concessionaria.mostrarListaDeCarros().map((carro: Carro) => {

    if(carro['modelo'] == cliente.dizerCarroPreferido()) {
        cliente.comprarCarro(carro);
    }
})

console.log(cliente.dizerCarroQueTem())