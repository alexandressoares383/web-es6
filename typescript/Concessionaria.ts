import { ConcessionariaInterface } from './ConcessionariaInterface';
import Carro from './Carro';

class Concessionaria implements ConcessionariaInterface {

    private endereco: string
    private listaDeCarros: Array<Carro>

    constructor(endereco: string, listaDeCarros: Array<Carro>) {
        this.endereco = endereco
        this.listaDeCarros = listaDeCarros
    }

    /** Métodos */
    public fornecerEndereco(): string {
        return this.endereco
    }

    public mostrarListaDeCarros(): Array<Carro> {
        return this.listaDeCarros
    }

    public fornecerHorariosDeFuncionamento(): string {
        return 'De segunda a sexta das 8:00 as 18:00 e sábado das 8:00 as 12:00'
    }

}

export default Concessionaria;