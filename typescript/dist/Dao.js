"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Dao = /** @class */ (function () {
    function Dao() {
        this.nomeTabela = '';
    }
    Dao.prototype.inserir = function (object) {
        console.log('Lógica do insert');
        return true;
    };
    Dao.prototype.atualizar = function (object) {
        console.log('Lógica do update');
        return true;
    };
    Dao.prototype.remover = function (id) {
        console.log('Lógica do delete');
        return Object();
    };
    Dao.prototype.selecionar = function (id) {
        console.log('Lógica do select');
        return Object();
    };
    Dao.prototype.selecionarTodos = function () {
        console.log('Lógica de getAll');
        return [Object()];
    };
    return Dao;
}());
exports.default = Dao;
