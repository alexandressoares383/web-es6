"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Pessoa_1 = require("./Pessoa");
var PessoaDao = /** @class */ (function () {
    function PessoaDao() {
        this.nomeTabela = 'tb_pessoa';
    }
    PessoaDao.prototype.inserir = function (object) {
        console.log('Lógica do insert');
        return true;
    };
    PessoaDao.prototype.atualizar = function (object) {
        console.log('Lógica do update');
        return true;
    };
    PessoaDao.prototype.remover = function (id) {
        console.log('Lógica do delete');
        return new Pessoa_1.default('', '');
    };
    PessoaDao.prototype.selecionar = function (id) {
        console.log('Lógica do select');
        return new Pessoa_1.default('', '');
    };
    PessoaDao.prototype.selecionarTodos = function () {
        console.log('Lógica de getAll');
        return [new Pessoa_1.default('', '')];
    };
    return PessoaDao;
}());
exports.default = PessoaDao;
