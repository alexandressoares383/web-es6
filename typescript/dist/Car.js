"use strict";
var Car = /** @class */ (function () {
    function Car(modelo, numeroDePortas, velocidade) {
        this.velocidade = 0;
        this.modelo = modelo;
        this.numeroDePortas = numeroDePortas;
    }
    Car.prototype.acelerar = function () {
        this.velocidade = this.velocidade;
    };
    Car.prototype.parar = function () {
        this.velocidade = 0;
    };
    Car.prototype.velocidadeAtual = function () {
        return this.velocidade;
    };
    return Car;
}());
