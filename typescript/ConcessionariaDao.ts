import { DaoInterface } from "./DaoInterface";
import Concessionaria from "./Concessionaria";

class ConcessionariaDao implements DaoInterface {

    nomeTabela: string = 'tb_concessionaria'

    inserir(object: Concessionaria): boolean {
        console.log('Lógica do insert');
        return true
    }

    atualizar(object: Concessionaria): boolean {
        console.log('Lógica do update')
        return true
    }

    remover(id: number): Concessionaria {
        console.log('Lógica do delete')
        return new Concessionaria('', [])
    }

    selecionar(id: number): Concessionaria {
        console.log('Lógica do select')
        return new Concessionaria('', [])
    }
    
    selecionarTodos(): any {
        console.log('Lógica de getAll')
        return [new Concessionaria('', [])]
    }

}

export default ConcessionariaDao;