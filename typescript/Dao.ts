import { DaoInterface } from './DaoInterface';

class Dao<T> implements DaoInterface<T> {

    nomeTabela: string = ''

    inserir(object: T): boolean {
        console.log('Lógica do insert');
        return true
    }

    atualizar(object: T): boolean {
        console.log('Lógica do update')
        return true
    }

    remover(id: number): T {
        console.log('Lógica do delete')
        return Object()
    }

    selecionar(id: number): T {
        console.log('Lógica do select')
        return Object()
    }

    selecionarTodos(): [T] {
        console.log('Lógica de getAll')
        return [Object()]
    }

}

export default Dao;