import { DaoInterface } from "./DaoInterface";
import Pessoa from "./Pessoa";

class PessoaDao implements DaoInterface {

    nomeTabela: string = 'tb_pessoa'

    inserir(object: Pessoa): boolean {
        console.log('Lógica do insert');
        return true
    }

    atualizar(object: Pessoa): boolean {
        console.log('Lógica do update')
        return true
    }

    remover(id: number): Pessoa {
        console.log('Lógica do delete')
        return new Pessoa('', '')
    }

    selecionar(id: number): Pessoa {
        console.log('Lógica do select')
        return new Pessoa('', '')
    }

    selecionarTodos(): any {
        console.log('Lógica de getAll')
        return [new Pessoa('', '')]
    }

}

export default PessoaDao;